MediaTek Yocto/OpenEmbedded Layer
=================================

Supported SoCs
--------------

This layer supports the following SoC:

* MT8183

Supported MACHINEs/boards
-------------------------

This layer supports the following MACHINEs/boards:

* MT8183 Pumpkin board (MACHINE = mt8183-pumpkin)

Setup build environment
-----------------------

	$ mkdir mediatek
	$ cd mediatek
	mediatek$ git clone --depth 1 git://git.yoctoproject.org/poky -b honister
	mediatek$ git clone --depth 1 git://git.yoctoproject.org/meta-arm -b honister
	mediatek$ git clone https://gitlab.com/baylibre/mediatek/meta-mediatek.git -b honister
	mediatek$ source poky/oe-init-build-env
	mediatek/build$ bitbake-layers add-layer ../meta-arm/meta-arm-toolchain
	mediatek/build$ bitbake-layers add-layer ../meta-arm/meta-arm
	mediatek/build$ bitbake-layers add-layer ../meta-mediatek/

Building core-image-weston
--------------------------

	$ cd mediatek
	mediatek$ source poky/oe-init-build-env
	mediatek/build$ MACHINE=<machine> bitbake core-image-weston

Flashing
--------

	$ cd mediatek/build/tmp/deploy/images/<machine>
	mediatek/build/tmp/deploy/images/<machine>$ img2simg core-image-weston-<machine>.wic core-image-weston-<machine>.img
	mediatek/build/tmp/deploy/images/<machine>$ fastboot flash mmc0 core-image-weston-<machine>.img
	mediatek/build/tmp/deploy/images/<machine>$ fastboot flash mmc0boot1 u-boot-env.bin
