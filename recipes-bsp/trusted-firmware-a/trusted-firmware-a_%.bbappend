FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"
COMPATIBLE_MACHINE = "mt*"

DEPENDS:append = " ${@bb.utils.contains('TFA_SPD', "opteed", "optee-os", "", d)}"

EXTRA_OEMAKE += "${@bb.utils.contains('TFA_UBOOT', '1', 'NEED_BL33=yes', '',d)}"

OPTEE_EXTRA_OEMAKE = " \
	BL32=${STAGING_DIR_TARGET}/${nonarch_base_libdir}/firmware/tee.bin \
	NEED_BL32=yes \
"

EXTRA_OEMAKE += "${@bb.utils.contains('TFA_SPD', 'opteed', '${OPTEE_EXTRA_OEMAKE}', '',d)}"
