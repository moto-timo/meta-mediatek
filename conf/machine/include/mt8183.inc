require conf/machine/include/arm/armv8a/tune-cortexa73-cortexa53.inc
require conf/machine/include/soc-family.inc
require mediatek-common.inc

SOC_FAMILY = "mt8183:mt8385:i500"

# OPTEE OS
OPTEE_TARGET = "mediatek-mt8183"

# TF-A
TFA_PLATFORM = "mt8183"
TFA_BUILD_TARGET = "bl31 fip"
TFA_UBOOT = "1"
TFA_SPD = "opteed"
