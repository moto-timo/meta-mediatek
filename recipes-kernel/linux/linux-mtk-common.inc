# Copyright (C) 2020 Fabien Parent <fparent@baylibre.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-kernel/linux/linux-yocto.inc
inherit kernel-fitimage

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

LINUX_VERSION_EXTENSION = "-mtk"

S = "${WORKDIR}/git"

# SoC specific config fragments
SRC_URI:append:mt8183 = " file://mt8183.cfg "

KERNEL_EXTRA_ARGS = "Image.gz dtbs"
KCONFIG_MODE = "--alldefconfig"
KBUILD_DEFCONFIG = "defconfig"

PV = "${LINUX_VERSION}+git${SRCPV}"
