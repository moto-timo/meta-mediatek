require u-boot-scr.inc

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

RDEPENDS:${PN} += "${PN}-scr"
DEPENDS += "${PN}-tools-native"

SRC_URI += " \
	file://fw_env.config \
	file://boot.script \
	file://mtk.cfg \
	file://0001-HACK-usb-mtu3-don-t-fail-on-unstable-sts1-clk.patch \
"

do_deploy:append() {
	uboot-mkenvimage -s 4096 -o ${DEPLOYDIR}/u-boot-env.bin \
		${DEPLOYDIR}/u-boot-initial-env
}
