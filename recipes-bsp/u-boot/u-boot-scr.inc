DEPENDS += "u-boot-tools-native"

do_compile:append() {
	uboot-mkimage -A arm -T script -O linux -d ${WORKDIR}/boot.script \
		${WORKDIR}/boot.scr
}

PACKAGE_BEFORE_PN += "${PN}-scr"

SYSROOT_DIRS += " /boot"
FILES:${PN}-scr = " \
    /boot/boot*.scr \
"

RDEPENDS:${PN} += "${PN}-scr"
