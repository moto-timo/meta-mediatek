# Kernel
KERNEL_IMAGETYPE = "fitImage"

# U-Boot
UBOOT_SUFFIX = "bin"
UBOOT_ENV_SUFFIX = "scr"
UBOOT_ENV = "boot"
UBOOT_ENTRYPOINT       = "0x40200000"
UBOOT_LOADADDRESS      = "0x40200000"
UBOOT_DTB_LOADADDRESS  = "0x44000000"

IMAGE_BOOT_FILES = " \
	${UBOOT_ENV}.${UBOOT_ENV_SUFFIX} \
	${KERNEL_IMAGETYPE} \
"

IMAGE_FSTYPES ?= "ext4 wic"
WKS_FILE ?= "mediatek.wks"

SERIAL_CONSOLES = "921600;ttyS0"

PREFERRED_PROVIDER_virtual/kernel ??= "linux-mtk"
PREFERRED_PROVIDER_virtual/bootloader ??= "u-boot"

MACHINE_EXTRA_RDEPENDS += " \
	kernel-modules \
	kernel-devicetree \
	trusted-firmware-a \
	u-boot-scr \
"
