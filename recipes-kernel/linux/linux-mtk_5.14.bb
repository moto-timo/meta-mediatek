# SPDX-License-Identifier: MIT
# Copyright (C) 2021 Fabien Parent <fparent@baylibre.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require linux-mtk-common.inc

KBRANCH = "linux-5.14.y"
LINUX_VERSION ?= "5.14.6"

SRC_URI = "git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git;protocol=https;branch=${KBRANCH}"
SRCREV = "6a7ababc0268063d0798c46d5859a90ee996612f"
